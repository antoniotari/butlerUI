package com.mokalab.butler.image;

import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.mokalab.butler.listenermodel.PostableHandler;
import com.mokalab.butler.listenermodel.ThreadPool;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.widget.ImageView;

import javax.inject.Inject;

import static com.mokalab.butler.injection.ApplicationGraph.getObjectGraph;

/**
 * Center crops and then rounds the corners.
 *
 * Created by josh.allen@digiflare.com on 14-12-21
 */
public class RoundedCornerCallback extends BitmapAjaxCallback {

    private final int mWidth;
    private final int mHeight;
    private final int mRadius;
    @Inject
    ThreadPool mThreadPool;

    public RoundedCornerCallback(int width, int height, int radius) {
        getObjectGraph().inject(this);
        mWidth = width;
        mHeight = height;
        mRadius = radius;
    }

    @Override
    public void callback(String url, final ImageView view, final Bitmap bm, AjaxStatus status) {
        mThreadPool.post(new Runnable() {
            @Override
            public void run() {
                roundBitmap(view, bm);
            }
        });
    }

    private void roundBitmap(final ImageView view, Bitmap bm) {
        Bitmap resized = ThumbnailUtils.extractThumbnail(bm, mWidth, mHeight);
        final Bitmap rounded = new BitmapCorners().getRoundedCornerBitmap(resized, mRadius);
        PostableHandler.UI_THREAD.post(new Runnable() {
            @Override
            public void run() {
                view.setImageBitmap(rounded);
            }
        });
    }
}
