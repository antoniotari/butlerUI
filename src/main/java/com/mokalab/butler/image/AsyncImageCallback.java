package com.mokalab.butler.image;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by Antonio Tari on 03/12/14.
 */
public interface AsyncImageCallback {
    public void callback(String url, ImageView imageView, Bitmap bitmap);
}
