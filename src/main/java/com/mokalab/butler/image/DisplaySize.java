package com.mokalab.butler.image;

import com.mokalab.butler.injection.ForApplication;

import android.content.Context;
import android.content.res.Configuration;
import android.util.DisplayMetrics;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.mokalab.butler.injection.ApplicationGraph.getObjectGraph;

/**
 * Created by josh.allen@digiflare.com on 15-01-09
 */
@Singleton
public class DisplaySize {

    @Inject @ForApplication
    Context mContext;

    @Inject
    public DisplaySize() {
        getObjectGraph().inject(this);
        ScreenDimension.setMetrics(mContext);
    }

    public int getWidth() {
        // ScreenDimension always returns Portrait values for width.
        if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            return ScreenDimension.getScreenWidthPX();
        } else {
            return ScreenDimension.getScreenHeightPX();
        }
    }

    public int getHeight() {
        // ScreenDimension always returns Portrait values for height.
        if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            return ScreenDimension.getScreenHeightPX();
        } else {
            return ScreenDimension.getScreenWidthPX();
        }
    }


    public float getDensityRatio() {
        return ScreenDimension.getDensityDpi() / (float) DisplayMetrics.DENSITY_DEFAULT;
    }

}
