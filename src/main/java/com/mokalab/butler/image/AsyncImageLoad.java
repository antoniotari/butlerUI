package com.mokalab.butler.image;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.util.AQUtility;
import com.mokalab.butler.injection.ApplicationGraph;

import android.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import javax.inject.Inject;

//import com.squareup.picasso.Callback;

/**
 * Created by Antonio Tari on 2014-07-08.
 */
public class AsyncImageLoad {

    public static final String RESOURCE_ID_PREFIX = "resourceId:";

    private static final int AQUERY_NETWORKLIMIT = 16;
    private static final int AQUERY_ICONCACHELIMIT = 144;
    private static final int AQUERY_CACHELIMIT = 144;
    private static final int AQUERY_PIXELLIMIT = 400 * 400;
    private static final int AQUERY_MAXPIXELLIMIT = 2000000;

    @Inject
    AQuery mAQuery;

    private AlphaAnimation mFadeIn;

    private String mFallbackImageUrl;
    private String mFallbackImageId;

    public enum ImageFadeIn{
        STATIC,
        LENGTH_SHORT,
        LENGTH_LONG,
        LENGTH_MED;

        public static int getDuration(ImageFadeIn imageFadeIn){
            int duration;
            switch (imageFadeIn){
                case STATIC:
                    duration = 0;
                    break;
                case LENGTH_SHORT:
                    duration = 222;
                    break;
                case LENGTH_LONG:
                    duration = 1000;
                    break;
                case LENGTH_MED:
                default:
                    duration = 555;
            }
            return duration;
        }
    }

    /**
     * builder method for the fallback image
     * @param url
     * @return
     */
    public AsyncImageLoad fallbackImageUrl(String url){
        mFallbackImageUrl=url;
        return this;
    }

    /**
     * builder method for the fallback image id
     */
    public AsyncImageLoad fallbackImageId(int drawableId){
        mFallbackImageId=RESOURCE_ID_PREFIX + drawableId;
        return this;
    }

    private final AsyncImageCallback mDefaultCallback = new AsyncImageCallback() {
        @Override
        public void callback(final String url, final ImageView imageView, final Bitmap bitmap) {
            imageView.setImageBitmap(bitmap);
        }
    };

    public AsyncImageLoad() {
        ApplicationGraph.getObjectGraph().inject(this);
    }

    private void fadeInView(ImageView theView,ImageFadeIn imageFadeIn) {
        if (mFadeIn == null) {
            mFadeIn = new AlphaAnimation(0, 1);
            mFadeIn.setInterpolator(new DecelerateInterpolator()); //add this
            mFadeIn.setDuration(ImageFadeIn.getDuration(imageFadeIn));
        }
        theView.setAnimation(mFadeIn);
        mFadeIn.start();
    }

    /**
     * this method initializes whatever system used to async load images this should go in the Application class
     */
    public static void initialize(Context context) {

        //VolleyImageHelper.init(context,16000);

        //set the max number of concurrent network connections, default is 4
        AjaxCallback.setNetworkLimit(AQUERY_NETWORKLIMIT);

        //set the max number of icons (image width <= 50) to be cached in memory, default is 20
        BitmapAjaxCallback.setIconCacheLimit(AQUERY_ICONCACHELIMIT);

        //set the max number of images (image width > 50) to be cached in memory, default is 20
        BitmapAjaxCallback.setCacheLimit(AQUERY_CACHELIMIT);

        //set the max size of an image to be cached in memory, default is 1600 pixels (ie. 400x400)
        BitmapAjaxCallback.setPixelLimit(AQUERY_PIXELLIMIT);

        //set the max size of the memory cache, default is 1M pixels (4MB)
        BitmapAjaxCallback.setMaxPixelLimit(AQUERY_MAXPIXELLIMIT);
    }
    //-----------------------------------------------------------------------------------------------

    /**
     * all the actions to be performed when closing the app put this onDestroy of the main activity or in onMemoryLow of the Application
     * class
     */
    public static void close(Context context) {
        long triggerSize = 3000000;
        long targetSize = 2000000;
        AQUtility.cleanCacheAsync(context, triggerSize, targetSize);
    }

    /**
     * Loads an image from a url inside the image view
     */
    public void loadImageFromUrl(View theView, String image_path, boolean memoryCache, boolean fileCache
            ,final AsyncImageCallback asyncCallback){
        loadImageFromUrl(theView, image_path, memoryCache, fileCache, ImageFadeIn.LENGTH_MED, asyncCallback);
    }

    /**
     * Loads an image from a url inside the image view
     */
    public void loadImageFromUrl(View theView, String image_path, boolean memoryCache, boolean fileCache
            ,final ImageFadeIn fadeInDuration
            ,final AsyncImageCallback asyncCallback) {

        if (theView == null) return;

        BitmapAjaxCallback ajaxCallback = new BitmapAjaxCallback() {
            @Override
            public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                if(status.getCode()==AjaxStatus.AUTH_ERROR||status.getCode()==AjaxStatus.NETWORK_ERROR||status.getCode()==AjaxStatus.TRANSFORM_ERROR
                        ||bm==null){
                    if(mFallbackImageUrl!=null) {
                        mAQuery.id(iv).image(mFallbackImageUrl);
                    } else if(mFallbackImageId!=null) {
                        mAQuery.id(iv).image(mFallbackImageId);
                    }
                    return;
                }

                asyncCallback.callback(url, iv, bm);
                if(status.getCode()==AjaxStatus.NETWORK) {
                    fadeInView(iv, fadeInDuration);
                }
            }
        }
                //setup the callback
                .url(image_path)
                        //.animation(AQuery.FADE_IN)
                .memCache(memoryCache)
                .fileCache(fileCache);

        mAQuery.id(theView).image(ajaxCallback);
    }

    /**
     * Loads an image from a url inside the image view, memory cache and file cache are automatically enabled
     */
    public void loadImageFromUrl(View theView, String image_path) {
        loadImageFromUrl(theView, image_path, ImageFadeIn.LENGTH_MED);
    }

    /**
     * Loads an image from a url inside the image view, memory cache and file cache are automatically enabled
     */
    public void loadImageFromUrl(View theView, String image_path,ImageFadeIn imageFadeInDuration) {
        loadImageFromUrl(theView, image_path, true, true, imageFadeInDuration, mDefaultCallback);
    }

    /**
     * Loads an image from a url inside the image view, memory cache and file cache are automatically enabled
     */
    public void loadImageFromUrl(View theView, String image_path,final AsyncImageCallback asyncCallback) {
        loadImageFromUrl(theView, image_path,true,true, ImageFadeIn.LENGTH_MED,asyncCallback);
    }

//    /**
//     * Loads an image from a url inside the image view
//     */
//    public static void loadImageFromUrl(View theView, String image_path, boolean memoryCache, boolean fileCache,
//            final AsyncImageCallback asyncCallback) {
//        if (image_path == null || image_path.isEmpty()) {
//            return;
//        }
//        if (theView == null) {
//            return;
//        }
//
//        BitmapAjaxCallback ajaxCallback = new BitmapAjaxCallback() {
//            @Override
//            public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
//                asyncCallback.callback(url, iv, bm);
//            }
//        }
//                //setup the callback
//                .url(image_path)
//                        //.animation(AQuery.FADE_IN)
//                .memCache(memoryCache)
//                .fileCache(fileCache);
//
//        new AQuery(theView.getContext().getApplicationContext()).id(theView).image(ajaxCallback);
//    }

    //-----------------------------------------------------------------------------------------------
    public static void loadImageFromUrl(Context context, View theView, String image_path, int width, int height, int resDefaultIcon,
            float ratio) {

        if (theView == null || context == null) {
            return;
        }

//        ImageLoader imageLoader = VolleyImageHelper.getImageLoader();
//        imageLoader.get(image_path,ImageLoader.getImageListener((ImageView)theView,resDefaultIcon, resDefaultIcon));
//        if(true)return;

//        if(Build.VERSION.SDK_INT==Build.VERSION_CODES.KITKAT)
//        {
//            //Log.debug("USING PICASSO");
//            Picasso.with(context)
//                    .load(image_path)//.transform(new CropSquareTransformation())
//                    .resize(width, height)
//                    .centerCrop()
//                            //.placeholder(R.drawable.defaulticon_large)
//                    .error(resDefaultIcon)
//                    .into(
//                            (theView instanceof ImageView)?((ImageView)theView):
//                                    //TODO check other types
//                                    ((ImageButton)theView)
//                    );
//        }
//        else
        {
            new AQuery(context)
                    .id(theView)
                    .image(
                            image_path,
                            true,
                            true,
                            width,
                            resDefaultIcon,
                            null,
                            AQuery.FADE_IN,
                            ratio);
        }
    }

    //-----------------------------------------------------------------------------------------------
    public static void loadImageFromUrl(Context context, View theView, String image_path, int width, int height, int resDefaultIcon) {

        if (theView == null || context == null) {
            return;
        }

        new AQuery(context)
                .id(theView)
                .image(
                        image_path,
                        true,
                        true,
                        width,
                        resDefaultIcon,
                        null,
                        AQuery.FADE_IN);
        // ratio);
    }

    //-----------------------------------------------------------------------------------------------
    public static void loadImageFromUrl(Context context, View theView, String image_path, int width, int height) {
        if (theView == null || context == null) {
            return;
        }

        float ratio = ((float) width) / ((float) height);

        BitmapAjaxCallback ajaxCallback = new BitmapAjaxCallback() {
            @Override
            public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        }
                //setup the callback
                .url(image_path)
                .animation(AQuery.FADE_IN)
                .memCache(true)
                .targetWidth(width)
                .ratio(ratio)
                .fileCache(true);

        new AQuery(theView.getContext().getApplicationContext()).id(theView).image(ajaxCallback);
    }

    //-----------------------------------------------------------------------------------------------
    public static void loadImageFromUrlFullScreen(Context context, View theView, String image_path) {

        if (theView == null || context == null) {
            return;
        }
        BitmapAjaxCallback ajaxCallback = new BitmapAjaxCallback() {
            @Override
            public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            }
        }
                //setup the callback
                .url(image_path)
                .animation(AQuery.FADE_IN)
                .memCache(true)
                .fileCache(true);

        new AQuery(theView.getContext().getApplicationContext()).id(theView).image(ajaxCallback);
    }

    //-----------------------------------------------------------------------------------------------
    public static void loadImageFromUrl(Context context, View theView, String image_path) {
        if (theView == null || context == null) {
            return;
        }
        new AQuery(context)
                .id(theView)
                .image(image_path, true, true, 0, R.drawable.screen_background_light_transparent, null, AQuery.FADE_IN);
    }
}
