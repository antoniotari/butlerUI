package com.mokalab.butler.widgets;

import android.view.View;

/**
 * Created by pirdad on 12/23/2013.
 */
public interface TabClickListener {

    public void onTabSelected(View view, int position);

}
