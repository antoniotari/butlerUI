package com.mokalab.butler.widgets;

import android.view.View;

/**
 * Created by pirdad on 12/22/2013.
 */
public interface PagerAdapterCustomViewProvider {

    public View getCustomView(int position);
}
