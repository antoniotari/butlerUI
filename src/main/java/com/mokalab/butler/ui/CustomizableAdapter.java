package com.mokalab.butler.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mokalab.butler.ui.templates.LayoutBase;
import com.mokalab.butler.ui.templates.TemplateArguments;
import com.mokalab.butler.util.Log;

import org.apache.commons.lang3.Validate;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by josh.allen@digiflare.com on 15-02-11
 */
public class CustomizableAdapter extends BaseAdapter {

    /**
     * Values for getItemViewType(). Must be between 0 and getViewTypeCount().
     */
    private static final int EMPTY_LAYOUT_ID = 0;
    private static final int GRID_LAYOUT_ID = 1;

    private final Callable<LayoutBase> mLayoutCreator;
    private final Context mContext;
    private List<? extends TemplateArguments> mData;
    private int mTopPadding;
    private int mNumColumns;
    private Integer mWidth = null;
    private Integer mHeight = null;

    public CustomizableAdapter(Context context, Callable<LayoutBase> layoutCreator) {
        Validate.notNull(context, "context");
        Validate.notNull(layoutCreator, "layoutCreator");
        mContext = context;
        mLayoutCreator = layoutCreator;
    }

    public List<? extends TemplateArguments> getData() {
        return mData;
    }

    public void setData(List<? extends TemplateArguments> data) {
        mData = data;
    }

    public void useTopRowForPadding(int numColumns, int topPadding) {
        if (topPadding > 0 && numColumns <= 0) {
            throw new IllegalArgumentException("Cannot set topPadding "+topPadding+" without number of columns"+numColumns);
        }
        mTopPadding = topPadding;
        mNumColumns = numColumns;
    }

    public Integer getWidth() {
        return mWidth;
    }

    public void setWidth(Integer width) {
        mWidth = width;
    }

    public Integer getHeight() {
        return mHeight;
    }

    public void setHeight(Integer height) {
        mHeight = height;
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size() + getBlankCellCount();
    }

    private int getBlankCellCount() {
        return mTopPadding > 0 ? mNumColumns : 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (position < getBlankCellCount()) {
                return createEmptyView(convertView, parent);
            }
            position -= getBlankCellCount();
            if (convertView == null) {
                LayoutBase layout = mLayoutCreator.call();
                Log.debug("Created layout for position ", position, layout);
                if (layout.getCount() != 1) {
                    throw new IllegalArgumentException("only layouts with 1 child are supported, not "+layout.getCount());
                }
                convertView = layout.getView(0, null, parent);
                if (mWidth != null) {
                    convertView.getLayoutParams().width = mWidth;
                }
                if (mHeight != null) {
                    convertView.getLayoutParams().height = mHeight;
                }
                convertView.setTag(R.id.tag_customizableAdapter_layout, layout);
            }
            LayoutBase layout = (LayoutBase) convertView.getTag(R.id.tag_customizableAdapter_layout);
            TemplateArguments args = mData.get(position);
            layout.getArgController().setArgs(args);
            return convertView;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private View createEmptyView(View convertView, ViewGroup parent) {
        if (convertView != null) {
            return convertView;
        }
        View empty = LayoutInflater.from(mContext).inflate(R.layout.template_empty_tile, parent, false);
        empty.getLayoutParams().height = mTopPadding;
        return empty;
    }

    public int getItemViewType(int position) {
        return position < getBlankCellCount() ? EMPTY_LAYOUT_ID : GRID_LAYOUT_ID;
    }

    public int getViewTypeCount() {
        // Note: this cannot change, even with a notifyDatasetChanged()
        return 2;
    }


    public int getTopPadding() {
        return mTopPadding;
    }
}
