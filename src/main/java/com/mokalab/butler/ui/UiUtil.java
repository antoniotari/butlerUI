package com.mokalab.butler.ui;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by antonio on 27/04/15.
 */
public class UiUtil {

    /**
     *
     * @param context       application context
     * @param fontFamily    String from the config with the name of the font
     * @return              Typeface generated according to the font family specified
     */
    public static Typeface decideTypeface(Context context, String fontFamily){
        Typeface returnTypeface=null;
        String[] keysArray = context.getResources().getStringArray(R.array.font_keys);
        String[] valuesArray = context.getResources().getStringArray(R.array.font_values);

        if(keysArray.length!=valuesArray.length){
            throw new RuntimeException("font_keys and font_values array (in the values resources) have different length");
        }

        int i=0;
        while(returnTypeface==null && i<keysArray.length){
            if(keysArray[i].equalsIgnoreCase(fontFamily)){
                returnTypeface=Typeface.createFromAsset(context.getAssets(), valuesArray[i]);
            }
            ++i;
        }

        return returnTypeface;
    }
}
