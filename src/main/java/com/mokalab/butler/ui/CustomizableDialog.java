package com.mokalab.butler.ui;

import com.mokalab.butler.error.ErrorInfo;
import com.mokalab.butler.image.DisplaySize;
import com.mokalab.butler.injection.ApplicationGraph;
import com.mokalab.butler.ui.templates.ActionName;
import com.mokalab.butler.ui.templates.LayoutBase;
import com.mokalab.butler.ui.templates.LayoutConfig;
import com.mokalab.butler.ui.templates.LayoutConfigParser;
import com.mokalab.butler.ui.templates.LayoutController;
import com.mokalab.butler.ui.templates.PageName;
import com.mokalab.butler.ui.templates.TemplateActionListener;
import com.mokalab.butler.ui.templates.TemplateArgController;
import com.mokalab.butler.ui.templates.TemplateArguments;
import com.mokalab.butler.ui.templates.ThemePageConfig;
import com.mokalab.butler.util.Log;

import org.apache.commons.lang3.Validate;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;

import javax.inject.Inject;

/**
 * Customizable dialog similar to CustomizableFragmentRedux.
 * Uses a LayoutConfigParser, pageId, templateArguments to layout the views on the dialog.
 *
 * Have the activity implement TemplateActionListener to be notified of template actions.
 *
 * Created by josh.allen@digiflare.com on 15-01-06
 */
public class CustomizableDialog extends DialogFragment implements TemplateActionListener {

    public enum Arguments {
        /** String */
        PAGE_ID,
        /** TemplateArguments */
        TEMPLATE_ARGS,
    }

    private String mPageId;
    private TemplateArguments mTemplateArgs;
    private TemplateActionListener mListener = null;
    private boolean mHasRoundedEdges = true;
    private boolean mHasCloseButton = false;

    @Inject
    LayoutConfigParser mConfigParser;
    @Inject
    LayoutConfig mLayoutConfig;
    @Inject
    DisplaySize mScreenSize;

    /**
     * Displays information in an ErrorInfo.
     * When in debug mode clicking on the dialog will display the debug information.
     * <br>
     * The requestId in the TemplateArguments passed back to the TemplateActionListener is
     * PageName.GENERAL_ERROR.getPageId()
     */
    public static CustomizableDialog newGeneralErrorDialog(ErrorInfo error) {
        Validate.notNull(error, "error");
        Log.error("Showing error dialog for", error);
        CustomizableDialog dialog = new CustomizableDialog();
        Bundle args = new Bundle();

        args.putString(Arguments.PAGE_ID.name(), PageName.GENERAL_ERROR.getPageId());

        TemplateArguments templateArgs = new TemplateArguments(PageName.GENERAL_ERROR.getPageId());
        templateArgs.setDisplayError(error);
        args.putParcelable(Arguments.TEMPLATE_ARGS.name(), templateArgs);

        dialog.setArguments(args);
        return dialog;
    }

    public static CustomizableDialog newCustomizableDialog(String pageId, TemplateArguments templateArguments) {
        Validate.notNull(pageId, "pageId");
        Validate.notNull(templateArguments, "templateArguments");
        CustomizableDialog dialog = new CustomizableDialog();


        Bundle args = new Bundle();
        args.putString(Arguments.PAGE_ID.name(), pageId);
        args.putParcelable(Arguments.TEMPLATE_ARGS.name(), templateArguments);
        dialog.setArguments(args);

        return dialog;
    }

    public CustomizableDialog() {
        ApplicationGraph.getObjectGraph().inject(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (TemplateActionListener) activity;
        } catch (ClassCastException ex) {
            throw (ClassCastException) new ClassCastException(activity + " must implement TemplateActionListener")
                    .initCause(ex);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageId = getArguments().getString(Arguments.PAGE_ID.name());
        Validate.notNull(mPageId, "pageId");
        mTemplateArgs = getArguments().getParcelable(Arguments.TEMPLATE_ARGS.name());
        Validate.notNull(mTemplateArgs, "templateArguments");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        try {
            Dialog dialog = new Dialog(getActivity(), R.style.dialog_generalError_text);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater inflater = getActivity().getLayoutInflater();
            ViewGroup root = (ViewGroup) inflater.inflate(R.layout.general_error_dialog, null);
            ThemePageConfig theme = mLayoutConfig.getTheme(mPageId);

            JSONObject config = mLayoutConfig.getLayout(mPageId);
            Log.debug("Showing dialog for page", mPageId, "with config", config);

            LayoutController controller = new LayoutController(root, getFragmentManager(), this, theme);
            ListView list = (ListView) root.findViewById(R.id.dialog_generalError_list);
            LayoutBase layoutAdapter = mConfigParser.parse(getActivity(), config, controller, mLayoutConfig, new TemplateArgController(mTemplateArgs));
            Log.debug("Config parsed to", layoutAdapter);
            list.setAdapter(layoutAdapter);

            dialog.setContentView(root);

            ImageView closeButton = (ImageView)root.findViewById(R.id.dialog_generalError_close);
            if( mHasCloseButton ) {
                closeButton.setVisibility( View.VISIBLE );

                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }

            // For match_parent to work on the children the width has to be set here explicitly.
            // By default it is wrap_content, which for some reason causes the list to be expanded correctly to the
            // full width of the screen, but the children to be expanded as wrap_content, even when their width is set
            // to match_parent.
            ViewGroup.LayoutParams params = dialog.getWindow().getAttributes();
            int width = (int) (mScreenSize.getWidth() * .75);
            dialog.getWindow().setLayout(width, params.height);
            // Set the background on the window instead of the root view so that transparent backgrounds work.
            GradientDrawable background = new GradientDrawable();

            // Round the corners.
            // TODO make configurable by page theme. For the time being removing to make square dialog box
            if( mHasRoundedEdges ) {
                background.setCornerRadius(getResources().getDimensionPixelSize(R.dimen.dialog_generalError_cornerRadius));
            }

            if (theme.getBackgroundColor() != null) {
                background.setColor(theme.getBackgroundColor());
            }

            dialog.getWindow().setBackgroundDrawable(background);
            return dialog;
        } catch (Exception ex) {
            onError(new ErrorInfo.Builder()
                    .setDebugMessage("Unsupported error page")
                    .setException(ex)
                    .build(), mTemplateArgs);
            return createSimpleDialog();
        }
    }

    private Dialog createSimpleDialog() {
        return new AlertDialog.Builder(getActivity())
                .setTitle(mTemplateArgs.replaceText(":dialogTitle/"))
                .setMessage(mTemplateArgs.replaceText(":dialogMessage/"))
                .setPositiveButton(R.string.dialog_generalError_dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create();
    }

    @Override
    public void onError(ErrorInfo error, TemplateArguments args) {
        Log.error("Could not show dialog", error);
        mListener.onError(error, args);
    }

    @Override
    public void onAction(String actionId, TemplateArguments args) {
        if (ActionName.DISMISS.getActionId().equals(actionId)) {
            dismiss();
        }
        else {
            mListener.onAction(actionId, mTemplateArgs);
        }
    }

    @Override
    public void openPageLink(String pageId, TemplateArguments args) {
        mListener.openPageLink(pageId, args);
    }

    @Override
    public void openDialogPageLink(String pageId, TemplateArguments args) {
        // Don't do anything
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        mListener.onAction(ActionName.DISMISS.getActionId(), mTemplateArgs);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mListener.onAction(ActionName.DISMISS.getActionId(), mTemplateArgs);
    }

    public void setHasRoundedEdges( boolean hasRoundedEdges ) {
        mHasRoundedEdges = hasRoundedEdges;
    }

    public void setHasCloseButton( boolean hasCloseButton ) {
        mHasCloseButton = hasCloseButton;
    }
}
