package com.mokalab.butler.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;

import java.util.Set;

/**
 * Adds a border around a background.
 *
 * Created by josh.allen@digiflare.com on 14-12-10
 */
public class BorderedBackground extends LayerDrawable {

    private final Resources mResources;
    private final GradientDrawable mGradient;

    private Integer mBorderWidth = null;
    private Integer mBorderColor = null;
    private Set<Sides> mSides = null;

    public BorderedBackground(Context context) {
        super(new Drawable[]{new GradientDrawable()});
        mResources = context.getResources();
        mGradient = (GradientDrawable) getDrawable(0);
    }

    public BorderedBackground setSolidColorId(int colorId) {
        mGradient.setColor(mResources.getColor(colorId));
        return this;
    }

    public BorderedBackground setSolidColor(int color) {
        mGradient.setColor(color);
        return this;
    }

    public BorderedBackground setBorderIds(int borderWidthId, int borderColorId) {
        return setBorder(mResources.getDimensionPixelSize(borderWidthId),
                         mResources.getColor(borderColorId));
    }

    public BorderedBackground setBorder(int borderWidth, int borderColor) {
        mBorderWidth = borderWidth;
        mBorderColor = borderColor;
        mGradient.setStroke(mBorderWidth, mBorderColor);
        if (mSides != null) {
            setSides(mSides);
        }
        return this;
    }

    public BorderedBackground setSides(Set<Sides> sides) {
        mSides = sides;
        if (mBorderWidth != null) {
            int left = sides.contains(Sides.LEFT) ? 0 : -mBorderWidth;
            int top = sides.contains(Sides.TOP) ? 0 : -mBorderWidth;
            int right = sides.contains(Sides.RIGHT) ? 0 : -mBorderWidth;
            int bottom = sides.contains(Sides.BOTTOM) ? 0 : -mBorderWidth;
            setLayerInset(0, left, top, right, bottom);
        }
        return this;
    }

}
