package com.mokalab.butler.ui;

import com.mokalab.butler.image.DisplaySize;
import com.mokalab.butler.image.ImageLoader;
import com.mokalab.butler.injection.ForApplication;
import com.mokalab.butler.ui.templates.ButlerTemplateModule;
import com.mokalab.butler.ui.templates.ErrorInfoTextReplacement;

import android.content.Context;

import java.util.Map;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by josh.allen@digiflare.com on 15-02-17
 */
@Module(
        injects = {
                DisplaySize.class,
                CustomizableDialog.class,
                ErrorInfoTextReplacement.class,
                ImageLoader.class,
        },
        includes = {
                ButlerTemplateModule.class
        },
        complete = false
)
public class ButlerUiModule {

    @Provides
    ImageLoader provideImageLoader(@ForApplication Context context, @Named("imageMap") Map<String,String> imageMap) {
        return new ImageLoader(context, imageMap);
    }
}
