package com.mokalab.butler.ui.templates;

/**
 * Created by josh.allen@digiflare.com on 14-12-18
 */
public interface DataSetChangedListener {

    void onDataSetChanged(TemplateArguments controller);
}
