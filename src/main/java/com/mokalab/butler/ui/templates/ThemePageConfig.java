package com.mokalab.butler.ui.templates;

import com.mokalab.butler.util.ShortToStringStyle;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;

import java.io.Serializable;
import java.util.AbstractList;

/**
 * Created by josh.allen@digiflare.com on 14-12-26
 */
public class ThemePageConfig implements Serializable {

    /**
     * Whether the background (mFullPageColors) should be a blurred image.
     */
    private boolean isDyanamicBackground;
    private int[] mNavBarColors;
    private Integer mBackgroundColor;
    private String mBackgroundImageId;
    private String mTitle;
    private String mActionBarIconImageId;

    public boolean isDyanamicBackground() {
        return isDyanamicBackground;
    }

    public void setDyanamicBackground(boolean isDyanamicBackground) {
        this.isDyanamicBackground = isDyanamicBackground;
    }

    public Integer getBackgroundColor() {
        return mBackgroundColor;
    }

    public void setBackgroundColor(Integer backgroundColor) {
        mBackgroundColor = backgroundColor;
    }

    public int[] getNavBarColors() {
        return mNavBarColors;
    }

    public void setNavBarColors(int[] navBarColors) {
        mNavBarColors = navBarColors;
    }

    public String getActionBarIconImageId() {
        return mActionBarIconImageId;
    }

    public void setActionBarIconImageId(String actionBarIconImageId) {
        mActionBarIconImageId = actionBarIconImageId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getBackgroundImageId() {
        return mBackgroundImageId;
    }

    public void setBackgroundImageId(String backgroundImageId) {
        mBackgroundImageId = backgroundImageId;
    }

    public Drawable getNavBarGradient() {
        if (mNavBarColors == null || mNavBarColors.length == 0) {
            return null;
        }
        if (mNavBarColors.length == 1) {
            return new ColorDrawable(mNavBarColors[0]);
        }
        ShapeDrawable.ShaderFactory sf = new ShapeDrawable.ShaderFactory() {
            @Override
            public Shader resize(int width, int height){
                LinearGradient lg = new LinearGradient(0, 0, width, height,
                        mNavBarColors,
                        null,
                        Shader.TileMode.REPEAT);
                return lg;
            }
        };

        PaintDrawable p=new PaintDrawable();
        p.setShape(new RectShape());
        p.setShaderFactory(sf);
        return p;
    }

    @Override
    public String toString() {
        ReflectionToStringBuilder builder = new ReflectionToStringBuilder(this, new ShortToStringStyle())
                .setExcludeFieldNames("mNavBarColors", "mBackgroundColor");
        if (mNavBarColors != null) {
            builder.append("navBarColors", new AbstractList<String>() {
                @Override
                public String get(int location) {
                    return Integer.toHexString(mNavBarColors[location]);
                }
                @Override
                public int size() {
                    return mNavBarColors.length;
                }
            });
        }
        if (mBackgroundColor != null) {
            builder.append("backgroundColor", Integer.toHexString(mBackgroundColor));
        }
        return builder.toString();
    }

}
