package com.mokalab.butler.ui.templates;

import org.jetbrains.annotations.NotNull;

/**
 * Known names for configurations LayoutConfig.getLayout()
 * Note that a String via getPageId() is used, rather than the enumeration itself, for the key because this allows new
 * values to be added without modifying this enum.
 *
 * Created by josh.allen@digiflare.com on 15-01-12
 */
public enum PageName {

    GENERAL_ERROR("generalError"),
    ;

    private final String mPageId;

    PageName(String pageId) {
        mPageId = pageId;
    }

    @NotNull
    public String getPageId() {
        return mPageId;
    }
}
