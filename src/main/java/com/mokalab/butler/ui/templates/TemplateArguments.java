package com.mokalab.butler.ui.templates;

import android.os.Parcel;
import android.os.Parcelable;

import com.mokalab.butler.error.ErrorInfo;
import com.mokalab.butler.util.Log;
import com.mokalab.butler.util.ShortToStringStyle;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by josh.allen@digiflare.com on 14-12-22
 */
public class TemplateArguments implements Parcelable, Cloneable {

    private static final Pattern mReplacementTagPattern = Pattern.compile(":\\w+/");
    private String mRequestId;
    private ErrorInfo mDisplayError;
    private List<TextReplacement> mExtraTextReplacements;

    /**
     * @param requestId Used to differentiate which request is making the callback, for activities that have more than
     *                  one fragment or dialog that are using TemplateActionListener. Used by TemplateActionDispatcher.
     */
    public TemplateArguments(String requestId) {
        Validate.notNull(requestId, "requestId");
        mRequestId = requestId;
        mExtraTextReplacements = new ArrayList<TextReplacement>();
    }

    public ErrorInfo getDisplayError() {
        return mDisplayError;
    }

    public void setDisplayError(ErrorInfo displayError) {
        mDisplayError = displayError;
    }

    protected @NotNull List<TextReplacement> getTextReplacements() {
        List<TextReplacement> replacements = new ArrayList<TextReplacement>(mExtraTextReplacements);
        replacements.add(new ErrorInfoTextReplacement(mDisplayError));
        return replacements;
    }

    public void addTextReplacement(TextReplacement replacement) {
        mExtraTextReplacements.add(replacement);
    }

    public String replaceText(String original) {
        Matcher matcher = mReplacementTagPattern.matcher(original);
        String replaced = original;
        while (matcher.find()) {
            String replace = null;
            String tag = original.substring(matcher.start(), matcher.end());
            List<TextReplacement> replacements = getTextReplacements();
            for (TextReplacement replacer : replacements) {
                replace = replacer.getReplacementFor(tag);
                //if (!StringUtils.isEmpty(replace)) {
                if( replace != null ) {
                    break;
                }
            }
            if (replace == null) {
                Log.debug("No text replacement found for", tag, "in", replacements);
                replace = tag;
            }
            replaced = replaced.replace(tag, replace);
        }
        return replaced;
    }

    public String getContentType() {
        return replaceText( ":mediaContentType/" ); //"Default";
    }

    /**
     * Used to differentiate which request is making the callback, for activities that have more than one fragment or
     * dialog that are using TemplateActionListener.
     */
    public String getRequestId() {
        return mRequestId;
    }

    /**
     * @return String suitable for debugging.
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, new ShortToStringStyle());
    }

    /**
     * Parcelable constructor.
     */
    protected TemplateArguments(Parcel in) {
        mDisplayError = in.readParcelable(ErrorInfo.class.getClassLoader());
        mRequestId = in.readString();
        mExtraTextReplacements = (List<TextReplacement>) in.readSerializable();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(mDisplayError, 0);
        out.writeString(mRequestId);
        out.writeSerializable(new ArrayList<TextReplacement>(mExtraTextReplacements));
    }

    public static final Creator<TemplateArguments> CREATOR = new Creator<TemplateArguments>() {

        @Override
        public TemplateArguments createFromParcel(Parcel in) {
            return new TemplateArguments(in);
        }

        @Override
        public TemplateArguments[] newArray(int size) {
            return new TemplateArguments[size];
        }
    };

    public TemplateArguments clone() throws CloneNotSupportedException {
        return (TemplateArguments)super.clone();
    }
}
