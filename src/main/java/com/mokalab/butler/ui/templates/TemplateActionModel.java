package com.mokalab.butler.ui.templates;

import com.mokalab.butler.error.ErrorInfo;

/**
 * TemplateActionListener that does nothing, to make it easier when you only want to override onAction().
 *
 * Created by josh.allen@digiflare.com on 15-02-02
 */
public class TemplateActionModel implements TemplateActionListener {
    @Override
    public void onError(ErrorInfo error, TemplateArguments args) {

    }

    @Override
    public void onAction(String actionId, TemplateArguments args) {

    }

    @Override
    public void openPageLink(String pageId, TemplateArguments args) {

    }

    @Override
    public void openDialogPageLink(String pageId, TemplateArguments args) {

    }
}
