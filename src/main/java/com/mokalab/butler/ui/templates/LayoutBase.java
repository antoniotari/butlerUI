package com.mokalab.butler.ui.templates;

import com.mokalab.butler.image.AsyncImageCallback;
import com.mokalab.butler.image.AsyncImageLoad;
import com.mokalab.butler.image.ImageLoader;
import com.mokalab.butler.listenermodel.PostableHandler;
import com.mokalab.butler.ui.R;
import com.mokalab.butler.util.Log;
import com.mokalab.butler.util.ShortToStringStyle;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.ToStringBuilder;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import javax.inject.Inject;

import static com.mokalab.butler.injection.ApplicationGraph.getObjectGraph;

/**
 * Every template from the config maps to a subclass of LayoutBase.
 *
 * Created by josh.allen@digiflare.com on 14-12-17
 */
public abstract class LayoutBase extends BaseAdapter implements DataSetChangedListener {

    public static final String JSON_KEY_DETAILS = "details";

    private Float mLayoutSize;
    private Float mWeight;
    private Padding mPadding = new Padding(0, 0, 0, 0);
    private Integer mWidth = null;
    private Integer mHeight = null;
    private boolean mIsLayoutVisible = true;
    private boolean mIsControlVisible = true;
    private boolean mIsToggleVisibilityOnParentClick = false;

    private final Context mContext;
    private final LayoutController mController;
    private TemplateArgController mArgController;

    private Integer mBackgroundColor;
    /**
     * action: "AddToPlaylist",
     */
    private String mAction;
    /**
     * pageLinkId: "playback-vod"
     */
    private String mPageLinkId;

    /**
     * For horizontal LinearLayout this maps to LinearLayout.LayoutParams.gravity
     * For RelativeLayout this is converted to RelativeLayout rules.
     */
    private int layoutGravity = Gravity.NO_GRAVITY;

    @Inject
    ImageLoader mImageLoader;

    @Inject
    AsyncImageLoad mAsyncImageLoad;

    public LayoutBase(Context context, LayoutController controller, TemplateArgController argController) {
        getObjectGraph().inject(this);
        Validate.notNull(context, "context");
        Validate.notNull(controller, "controller");
        Validate.notNull(argController, "argController");
        mContext = context;
        mController = controller;
        mArgController = argController;
    }

    public Float getLayoutSize() {
        return mLayoutSize;
    }

    public void setLayoutSize(Float layoutSize) {
        this.mLayoutSize = layoutSize;
    }

    public Float getWeight() {
        return mWeight;
    }

    public void setWeight(Float weight) {
        this.mWeight = weight;
    }

    public Integer getWidth() {
        return mWidth;
    }

    public void setWidth(Integer width) {
        mWidth = width;
    }

    public Integer getHeight() {
        return mHeight;
    }

    public void setHeight(Integer height) {
        mHeight = height;
    }

    public boolean isLayoutVisible() {
        return mIsLayoutVisible;
    }

    /**
     * Whether the layout should be visible based on config and what the parent knows.
     */
    public void setLayoutVisible(boolean isLayoutVisible) {
        mIsLayoutVisible = isLayoutVisible;
    }

    public boolean isControlVisible() {
        return mIsControlVisible;
    }

    public String getAction() {
        return mAction;
    }

    public void setAction(String action) {
        mAction = action;
    }

    public String getPageLinkId() {
        return mPageLinkId;
    }

    public void setPageLinkId(String pageLinkId) {
        mPageLinkId = pageLinkId;
    }

    public int getLayoutGravity() {
        return layoutGravity;
    }

    public void setLayoutGravity(int layoutGravity) {
        this.layoutGravity = layoutGravity;
    }

    /**
     * Whether the child think the control should be visible, for example if there is any text.
     */
    public void setControlVisible(final boolean isControlVisible) {
        if (isControlVisible != mIsControlVisible) {
            PostableHandler.UI_THREAD.post(new Runnable() {
                @Override
                public void run() {
                    mIsControlVisible = isControlVisible;
                    notifyDataSetChanged();
                }
            });
        }
    }

    public boolean isToggleVisibilityOnParentClick() {
        return mIsToggleVisibilityOnParentClick;
    }

    public void setToggleVisibilityOnParentClick(boolean isToggleVisibilityOnParentClick) {
        mIsToggleVisibilityOnParentClick = isToggleVisibilityOnParentClick;
    }

    public Integer getBackgroundColor() {
        return mBackgroundColor;
    }

    public void setBackgroundColor(Integer backgroundColor) {
        mBackgroundColor = backgroundColor;
    }

    protected Context getContext() {
        return mContext;
    }

    public LayoutController getController() {
        return mController;
    }

    public TemplateArgController getArgController() {
        return mArgController;
    }

    public void setArgController( TemplateArgController argController ) {
        mArgController = argController;
    }

    public void clonemArgController() {
        try {
            mArgController = new TemplateArgController(mArgController.getArgs().clone());
        } catch ( CloneNotSupportedException e ) {
            Log.hi( "CloneNotSupportedException" );
        }
    }

    public TemplateArguments getArgs() {
        return mArgController.getArgs();
    }

    protected void loadImage(ImageView imageView, String url) {
        //if(URLUtil.isValidUrl(url) && Patterns.WEB_URL.matcher(url).matches()) {
            mAsyncImageLoad.loadImageFromUrl(imageView, url, true, true, new AsyncImageCallback() {
                @Override
                public void callback(final String url2, final ImageView imageView2, final Bitmap bitmap) {
                    imageView2.setScaleType(ScaleType.FIT_CENTER);
                    imageView2.setImageBitmap(bitmap);
                }
            });
        //}else {
        //    mImageLoader.createAQuery()
        //            .id(imageView)
        //            .image(url, mImageLoader.getDefaultOptions());
        //}
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        if (convertView != null) {
            resetPadding(convertView);
        }
        final View view = getViewNoPadding(position, convertView, parent);
        applyPadding(position, view);
        applyLayoutSize(view);
        applyColor(view);

        // Add the DataSetChangedListener after the first call to getViewNoPadding, as that is when the base classes
        // have already have populated some of their UI components.
        mArgController.addDataSetChangedListener(this);

        return view;
    }

    private void addClickListener() {
        View parent = getController().getRootView();
        if (parent.getTag(R.id.tag_layoutBase_isClickListenerAdded) != null) {
            return;
        }
        if (parent instanceof AdapterView) {
            AdapterView list = (AdapterView) parent;
            final AdapterView.OnItemClickListener oldListener = list.getOnItemClickListener();
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    toggleLayoutVisibility();
                    if (oldListener != null) {
                        oldListener.onItemClick(parent, view, position, id);
                    }
                }
            });
        } else {
            parent.setClickable(true);
            parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleLayoutVisibility();
                }
            });
        }
        parent.setTag(R.id.tag_layoutBase_isClickListenerAdded, Boolean.TRUE);
    }

    private void toggleLayoutVisibility() {
        PostableHandler.UI_THREAD.post(new Runnable() {
            @Override
            public void run() {
                mIsLayoutVisible = !mIsLayoutVisible;
                notifyDataSetChanged();
            }
        });
    }

    private void resetPadding(View convertView) {
        Padding padding = (Padding) convertView.getTag(R.id.tag_layoutBase_padding);
        // Reset padding to original values.
        convertView.setPadding(padding.getPaddingLeft(), padding.getPaddingTop(),
                padding.getPaddingRight(), padding.getPaddingBottom());
    }

    private void applyPadding(int position, View view) {
        if (view.getTag(R.id.tag_layoutBase_padding) == null) {
            view.setTag(R.id.tag_layoutBase_padding, new Padding(view));
        }
        int paddingLeft = view.getPaddingLeft();
        int paddingTop = view.getPaddingTop();
        int paddingRight = view.getPaddingRight();
        int paddingBottom = view.getPaddingBottom();

        paddingLeft += mPadding.getPaddingLeft();
        paddingTop += mPadding.getPaddingTop();
        paddingRight += mPadding.getPaddingRight();
        paddingBottom += mPadding.getPaddingBottom();
        view.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }

    private void applyLayoutSize(View view) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        Log.debug("Setting view size to", sizeToString(mWidth), "x", sizeToString(mHeight), this);
        if (mHeight != null) {
            params.height = mHeight;
        }
        if (mWidth != null) {
            params.width = mWidth;
        }
    }

    private void applyColor(View view) {
        if (mBackgroundColor != null) {
            view.setBackgroundColor(mBackgroundColor);
        }
    }

    /**
     * @return View without any extra padding added other than what is from the inflated layout.
     */
    protected abstract View getViewNoPadding(int position, View convertView, ViewGroup parent);

    @Override
    public String toString() {
        // Note: do not use reflectionToString as subclasses would be too noisy.
        return new ToStringBuilder(this, new ShortToStringStyle())
                .append("size", mLayoutSize)
                .append("weight", mWeight)
                .append("padding", mPadding)
                .append("width", sizeToString(mWidth))
                .append("height", sizeToString(mHeight))
                .append("action", mAction)
                .toString();
    }

    public Padding getPadding() {
        return mPadding;
    }

    public void setPadding(Padding padding) {
        Validate.notNull(padding, "padding");
        mPadding = padding;
    }


    @Override
    public int getCount() {
        if (!mIsLayoutVisible || !mIsControlVisible) {
            if (mIsToggleVisibilityOnParentClick) {
                addClickListener();
            }
            return 0;
        }
        return getCountIfVisible();
    }

    /**
     * @return 1, override if you are returning more than one view to be displayed vertically.
     */
    public int getCountIfVisible() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    protected String visibilityToString(int visibility) {
        return visibility == View.VISIBLE ? "visible" : visibility == View.INVISIBLE ? "invisible" : "gone";
    }

    protected String sizeToString(Integer size) {
        if (size == null) {
            return null;
        }
        return size == ViewGroup.LayoutParams.MATCH_PARENT ? "match_parent" :
                size == ViewGroup.LayoutParams.WRAP_CONTENT ? "wrap_content" :
                        String.valueOf(size);
    }

    protected ImageLoader getImageLoader() {
        return mImageLoader;
    }

    /**
     * Calls onAction or openPageLink() on the ActionListener.
     */
    protected void processAction() {
        if (!StringUtils.isEmpty(mAction)) {
            getController().getActionListener().onAction(mAction, getArgs());
        } else if (!StringUtils.isEmpty(mPageLinkId)) {
            getController().getActionListener().openPageLink(mPageLinkId, getArgs());
        } else {
            Log.debug("No action associated with", this);
        }
    }

    /**
     * @return true if action or pageLinkId is set so that an onClickListener should be set.
     */
    protected boolean isActionable() {
        return !StringUtils.isEmpty(mAction) || !StringUtils.isEmpty(mPageLinkId);
    }
}
