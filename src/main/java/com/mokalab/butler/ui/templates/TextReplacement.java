package com.mokalab.butler.ui.templates;

import java.io.Serializable;

/**
 * Created by josh.allen@digiflare.com on 15-01-09
 */
public interface TextReplacement extends Serializable {

    String getReplacementFor(String name);
}
