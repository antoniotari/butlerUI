package com.mokalab.butler.ui.templates;

import dagger.Module;

/**
 * Created by josh.allen@digiflare.com on 15-01-09
 */
@Module(
        injects = {
                LayoutBase.class,
                LayoutConfigParser.class,
                LayoutController.class,
                TextControlTemplate.class,
                LayoutTemplate.class,
                ButtonTemplate.class,
                ButtonTextTemplate.class,
                HorizontalRuleTemplate.class,
                ImageTemplate.class,
                RelativeLayoutTemplate.class,
        },
        complete = false
)
public class ButlerTemplateModule {

}
