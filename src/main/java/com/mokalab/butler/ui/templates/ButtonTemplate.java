package com.mokalab.butler.ui.templates;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mokalab.butler.listenermodel.AsyncListenerModel;
import com.mokalab.butler.ui.R;

/**
 * Created by josh.allen@digiflare.com on 14-12-23
 */
public class ButtonTemplate extends LayoutBase {

    // Values from Config
    /**
     * imageId: "add-to-playlist",
     */
    private String mImageId;

    // View Holder
    private ImageView mButton = null;

    public ButtonTemplate(Context context, LayoutController controller, TemplateArgController argController) {
        super(context, controller, argController);
    }

    @Override
    public View getViewNoPadding(int position, View convertView, ViewGroup parent) {
        if (convertView != null) {
            return convertView;
        }
        View root = LayoutInflater.from(getContext()).inflate(R.layout.template_button, parent, false);
        mButton = (ImageView) root.findViewById(R.id.template_button_image);

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processAction();
            }
        });
        loadActionlImage();
        return root;
    }

    protected void loadActionlImage() {
        loadImage(mButton, mImageId);
    }

    public String getImageId() {
        return mImageId;
    }

    public void setImageId(String imageId) {
        // Preload the image.
        getImageLoader().loadBitmap(imageId, new AsyncListenerModel<Bitmap>());
        mImageId = imageId;
    }

    protected ImageView getButton() {
        return mButton;
    }

    @Override
    public void onDataSetChanged(TemplateArguments controller) {

    }
}
