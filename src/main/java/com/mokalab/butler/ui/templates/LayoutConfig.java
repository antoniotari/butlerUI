package com.mokalab.butler.ui.templates;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

/**
 * Created by josh.allen@digiflare.com on 15-01-09
 */
public interface LayoutConfig {

    @NotNull
    JSONObject getLayout(String pageId);

    @NotNull
    ThemePageConfig getTheme(String pageId);

}
