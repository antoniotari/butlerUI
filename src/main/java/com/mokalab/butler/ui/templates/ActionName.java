package com.mokalab.butler.ui.templates;

import org.jetbrains.annotations.NotNull;

/**
 * Known names for TemplateActionListener.onAction()
 *
 * Created by josh.allen@digiflare.com on 15-01-27
 */
public enum ActionName {

    DISMISS("Dismiss"),
    ;

    private final String mActionId;

    ActionName(String actionId) {
        mActionId = actionId;
    }

    @NotNull
    public String getActionId() {
        return mActionId;
    }

}
