package com.mokalab.butler.ui.templates;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mokalab.butler.ui.R;
import com.mokalab.butler.util.Log;
import com.mokalab.butler.util.ShortToStringStyle;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by josh.allen@digiflare.com on 14-12-17
 */
public class LayoutTemplate extends LayoutBase {

    protected List<LayoutBase> mContent = new ArrayList<LayoutBase>();
    private int orientation = -1; //LinearLayout.VERTICAL;
    private int gravity = Gravity.NO_GRAVITY;

    public LayoutTemplate(Context context, LayoutController controller, TemplateArgController argController) {
        super(context, controller, argController);
    }

    /**
     * @return Horizontal orientation: one LinearLayout with all children in it;
     *         Vertical orientation: one view for each child.
     */
    public View getViewNoPadding(int position, View convertView, ViewGroup parent) {
        if (isLinearLayout()) {
            if (convertView != null) {
                return convertView;
            }
            return createLinearLayout(parent);
        }
        LayoutPosition layoutPosition = positionToLayout(position);
        Log.debug("Getting position", layoutPosition.childPosition, "of", layoutPosition.child.getCount(), "from", layoutPosition.child);
        return layoutPosition.child.getView(layoutPosition.childPosition, convertView, parent);
    }

    protected View createLinearLayout(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        LinearLayout root = (LinearLayout) inflater.inflate(R.layout.template_layout, parent, false);
        root.setGravity(gravity);
        for (LayoutBase layout : mContent) {
            for (int i = 0; i < layout.getCount(); i++) {
                View child = layout.getView(i, null, root);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) child.getLayoutParams();
                params = adjustLayoutParams(layout, params);
                root.addView(child, params);
            }
        }

        if(orientation == -1) {
            root.setOrientation(LinearLayout.VERTICAL);
        } else {
            root.setOrientation(orientation);
        }
        return root;
    }

    private LinearLayout.LayoutParams adjustLayoutParams(LayoutBase layout, LinearLayout.LayoutParams params) {
        if (layout.getWeight() != null) {
            params.weight = layout.getWeight();
        }
        if (getLayoutSize() != null && orientation == LinearLayout.HORIZONTAL) {
            params.width = (int) (getController().getViewWidth(getController().getRootView()) * getLayoutSize());
        }
        params.gravity = layout.getLayoutGravity();
        return params;
    }

    public void addContent(LayoutBase content) {
        content.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                Log.debug("onChanged");
                notifyDataSetChanged();
            }
        });
        mContent.add(content);
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public int getGravity() {
        return gravity;
    }

    public void setGravity(int gravity) {
        this.gravity = gravity;
    }

    /**
     * @return 1, override if you are returning more than one view to be displayed vertically.
     */
    @Override
    public int getCountIfVisible() {
        if (isLinearLayout()) {
            return mContent.isEmpty() ? 0 : 1;
        }
        int count = 0;
        for (LayoutBase layout : mContent) {
            count += layout.getCount();
        }
        return count;
    }

    protected boolean isLinearLayout() {
        return orientation != -1;
    }

    protected LayoutPosition positionToLayout(int position) {
        int currentPosition = 0;
        int currentViewTypeId = 0;
        // TODO There is a bug here with recursion of a layout in a layout with 1 child Text node.
        // Position = 0, getCount = 1, currentPosition = 0, currentViewTypeID = 1
        for (int i = 0; i < mContent.size(); i++) {
            LayoutBase layout = mContent.get(i);
            if (currentPosition + layout.getCount() > position) {
                LayoutPosition found = new LayoutPosition();
                found.child = layout;
                found.childPosition = position - currentPosition;
                found.viewType = currentViewTypeId + layout.getItemViewType(found.childPosition);
                return found;
            }
            currentPosition += layout.getCount();
            currentViewTypeId += layout.getViewTypeCount();
        }
        throw new IndexOutOfBoundsException("position "+position+" >= count "+getCount());
    }

    public int getItemViewType(int position) {
        return positionToLayout(position).viewType;
    }

    public int getViewTypeCount() {
        int count = 0;
        for (LayoutBase layout : mContent) {
            count += layout.getViewTypeCount();
        }
        return count;
    }

    @Override
    public void onDataSetChanged(TemplateArguments controller) {

    }

    protected static class LayoutPosition {
        public LayoutBase child;
        public int childPosition;
        public int viewType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, new ShortToStringStyle())
                .appendSuper(super.toString())
                .append("orientation", orientationToString(orientation))
                .append("content", mContent)
                .toString();
    }

    private String orientationToString(int orientation) {
        return orientation == LinearLayout.HORIZONTAL ? "horizontal"
                : orientation == LinearLayout.VERTICAL ? "vertical" : String.valueOf(orientation);
    }

}
