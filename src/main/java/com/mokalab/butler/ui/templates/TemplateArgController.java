package com.mokalab.butler.ui.templates;

import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Shared by all layout controls that share the same template arguments.
 * This allows coordination between the layouts by example notifying when the data has changed.
 *
 * Created by josh.allen@digiflare.com on 15-02-13
 */
public class TemplateArgController {

    private TemplateArguments mArgs;
    private final Set<DataSetChangedListener> mDataSetChangedListeners =
            Collections.synchronizedSet(new HashSet<DataSetChangedListener>());

    public TemplateArgController(TemplateArguments args) {
        Validate.notNull(args, "args");
        mArgs = args;
    }

    public TemplateArguments getArgs() {
        return mArgs;
    }

    /**
     * Changes template arguments and notifies DataSetChangeListeners.
     */
    public void setArgs(TemplateArguments args) {
        Validate.notNull(args, "args");
        mArgs = args;
        notifyDataSetChanged();
    }

    public void addDataSetChangedListener(DataSetChangedListener listener) {
        mDataSetChangedListeners.add(listener);
    }

    public void removeDataSetChangedListener(DataSetChangedListener listener) {
        mDataSetChangedListeners.add(listener);
    }

    private void notifyDataSetChanged() {
        for (DataSetChangedListener listener :  new ArrayList<DataSetChangedListener>(mDataSetChangedListeners)) {
            listener.onDataSetChanged(mArgs);
        }
    }
}


