package com.mokalab.butler.ui.templates;

import com.antoniotari.android.jedi.Log;
import com.mokalab.butler.ui.R;
import com.mokalab.butler.ui.UiUtil;
import com.mokalab.butler.util.ShortToStringStyle;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.ToStringBuilder;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class TextControlTemplate extends LayoutBase {

    public static final String JSON_KEY_HEADINGTEXT = "headingText";
    public static final String JSON_KEY_URL = "url";

    // Config
    private String text;
    private Integer fontColor;
    private String fontFamily;
    private Float fontSize;
    private int fontStyle = Typeface.NORMAL;
    private int mGravity = Gravity.NO_GRAVITY;


    // ViewHolder
    protected TextView mTextView;

    public TextControlTemplate(Context context, LayoutController controller, TemplateArgController argController) {
        super(context, controller, argController);
    }

    @Override
    protected View getViewNoPadding(int position, View convertView, ViewGroup parent) {
        if( convertView != null ) {
            return convertView;
        }

        // Validate Items
        Validate.notNull(text, "text");
        Validate.notNull(fontColor, "fontColor");
        Validate.notNull(fontSize, "fontSize");

        // Get View
        LayoutInflater inflater = LayoutInflater.from(getContext());
        convertView=inflater.inflate(R.layout.template_text_control, parent, false);

        // Get Text View
        mTextView = (TextView) convertView.findViewById(R.id.templateTextViewDetails);//inflater.inflate(R.layout.template_text_control, parent, false);

        mTextView.setClickable(false);
        mTextView.setLongClickable(false);
        mTextView.setTextColor(fontColor);
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);

        Typeface face= UiUtil.decideTypeface(getContext(),fontFamily);
        mTextView.setTypeface(face, fontStyle);
        mTextView.setGravity(mGravity);
        Validate.notNull(mTextView.getLayoutParams());

        assignText(mTextView,text);
        return convertView;
    }

    public void refreshTextView() {
        if( mTextView != null ) {
            assignText(mTextView, text);
        }
    }

    protected void assignText(TextView textView,String textString) {
        String replaced = getArgs().replaceText(textString);

        if (replaced == null || replaced.isEmpty()) {
            textView.setVisibility(View.GONE);
            // Also call setControlVisible so that the space is removed from the ListView.
            setControlVisible(false);
        } else {
            if( replaced.contains("</") ){
                textView.setText(Html.fromHtml(replaced));
            } else {
                textView.setText(replaced);
            }
            textView.setVisibility(View.VISIBLE);
            setControlVisible(true);
        }
    }

    public TextView getTextView() {
        return mTextView;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getFontColor() {
        return fontColor;
    }

    public void setFontColor(Integer fontColor) {
        this.fontColor = fontColor;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public Float getFontSize() {
        return fontSize;
    }

    public void setFontSize(Float fontSize) {
        this.fontSize = fontSize;
    }

    public int getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(int fontStyle) {
        this.fontStyle = fontStyle;
    }

    public int getGravity() {
        return mGravity;
    }

    public void setGravity(int gravity) {
        mGravity = gravity;
    }

    @Override
    public String toString() {
        ToStringBuilder builder = new ToStringBuilder(this, new ShortToStringStyle())
                .appendSuper(super.toString())
                .append("text", text);
        if (mTextView != null) {
            builder.append("view.visible", visibilityToString(mTextView.getVisibility()));
        }
        return builder.toString();
    }

    @Override
    public void onDataSetChanged(TemplateArguments controller) {
        assignText(mTextView,text);
    }
}
