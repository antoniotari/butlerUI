package com.mokalab.butler.ui.templates;

import com.mokalab.butler.ui.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

/**
 * Created by josh.allen@digiflare.com on 15-01-30
 */
public class ImageTemplate extends LayoutBase {

    /**
     * imageId: "add-to-playlist",
     */
    private String mImageId;

    public ImageTemplate(Context context, LayoutController controller, TemplateArgController argController) {
        super(context, controller, argController);
    }

    public String getImageId() {
        return mImageId;
    }

    public void setImageId(String imageId) {
        mImageId = imageId;
    }

    @Override
    protected View getViewNoPadding(int position, View convertView, ViewGroup parent) {
        if (convertView != null) {
            return convertView;
        }
        LayoutInflater inflater = LayoutInflater.from(getContext());
        ImageView view = (ImageView) inflater.inflate(R.layout.template_image, parent, false);
        view.setScaleType(ScaleType.FIT_CENTER);
        loadImage(view, mImageId);
        return view;
    }

    @Override
    public void onDataSetChanged(TemplateArguments args) {
        // Space for rent.
    }
}
