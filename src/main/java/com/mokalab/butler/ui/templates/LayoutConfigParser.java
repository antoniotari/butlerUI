package com.mokalab.butler.ui.templates;

import com.mokalab.butler.image.DisplaySize;
import com.mokalab.butler.util.Log;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.Iterator;
import java.util.concurrent.Callable;

import javax.inject.Inject;

public class LayoutConfigParser {

    private final DisplaySize mScreenSize;
    //private final BuildInfo mBuildInfo;
    private Context mContext;
    private LayoutController mController;
    private TemplateArgController mArgController;
    private LayoutConfig mLayoutConfig;
    private boolean isDebug;



    @Inject
    public LayoutConfigParser(DisplaySize screenSize,boolean isDebug) {
        Validate.notNull(screenSize, "screenSize");
        //Validate.notNull(buildInfo, "buildInfo");
        mScreenSize = screenSize;
        this.isDebug=isDebug;
        //mBuildInfo = buildInfo;
    }

    protected Context getContext() {
        return mContext;
    }

    protected LayoutController getController() {
        return mController;
    }

    protected TemplateArgController getArgController() {
        return mArgController;
    }

    protected LayoutConfig getLayoutConfig() {
        return mLayoutConfig;
    }

    public LayoutBase parse(Context context,
                            JSONObject config,
                            LayoutController controller,
                            LayoutConfig layoutConfig,
                            TemplateArgController argController) {
        Validate.notNull(context, "context");
        Validate.notNull(controller, "controller");
        Validate.notNull(argController, "argController");
        mContext = context;
        mController = controller;
        mArgController = argController;
        mLayoutConfig = layoutConfig;

        return parseConfig(config);
    }


    private LayoutBase parseConfig(JSONObject config) {
        try {
            if (config.has("layout")) {
                JSONObject layout = config.getJSONObject("layout");
                if (layout.isNull("template")) {
                    return parseLayout(layout);
                } else {
                    return parseTemplate(layout);
                }
            } else if (config.has("contentControls")) {
                // Sometimes there is no layout, just directly contentControls.
                return parseLayout(config);
            }
            throw new IllegalArgumentException("Unknown root template in config "+
                    StringUtils.abbreviate(config.toString(), 500));
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    private LayoutBase parseLayout(JSONObject config) throws JSONException {
        String orientation = config.optString("orientation");
        if ("relative".equals(orientation)) {
            return parseRelativeLayout(config);
        } else {
            return parseLinearLayout(new LayoutTemplate(mContext, mController, mArgController), config);
        }
    }

    private LayoutBase parseRelativeLayout(JSONObject config) throws JSONException {
        RelativeLayoutTemplate layout = new RelativeLayoutTemplate(mContext, mController, mArgController);


        // contentControls
        JSONArray contentControls = config.getJSONArray("contentControls");
        for (int i = 0; i < contentControls.length(); i++) {
            JSONObject templateJson = contentControls.getJSONObject(i);
            if (templateJson.optBoolean("isDebugOnly", false) && !isDebug ) {
                Log.debug("Skipping content", i, " because isDebugOnly");
                continue;
            }
            LayoutBase template = parseTemplate(templateJson);
            layout.addContent(template);
        }

        return layout;
    }

    protected LayoutTemplate parseLinearLayout(LayoutTemplate layout, JSONObject config) {
        try {
            // orientation
            String orientation = config.optString("orientation");
            if ("horizontal".equals(orientation)) {
                layout.setOrientation(LinearLayout.HORIZONTAL);
            } else if ("vertical".equals(orientation)) {
                layout.setOrientation(LinearLayout.VERTICAL);
            }

            int gravity = Gravity.NO_GRAVITY;
            for (String desc : config.optString("gravity").split("\\|")) {
                if (desc.length() > 0) {
                    gravity |= gravityDescToInt(desc);
                }
            }
            layout.setGravity(gravity);

            setCommonElements(config, layout);

            // contentControls
            JSONArray contentControls = config.optJSONArray("contentControls");
            if( contentControls != null ) {
                for (int i = 0; i < contentControls.length(); i++) {
                    JSONObject templateJson = contentControls.getJSONObject(i);
                    if (templateJson.optBoolean("isDebugOnly", false) && !isDebug) {
                        Log.debug("Skipping content", i, " because isDebugOnly");
                        continue;
                    }
                    LayoutBase template = parseTemplate(templateJson);
                    layout.addContent(template);
                }
            }

            return layout;
        } catch (JSONException ex) {
            throw new RuntimeException("Error processing layout template "
                    +StringUtils.abbreviate(config.toString(), 500), ex);
        }
    }

    protected LayoutBase parseTemplate(JSONObject config) throws JSONException {
        String type = config.optString("template", null);
        if (type != null) {
            LayoutBase parsed = createLayoutForTemplateType(type, config);
            if (parsed != null) {
                setCommonElements(config, parsed);
                return parsed;
            }
        } else {
            JSONObject layout = config.optJSONObject("layout");
            if (layout != null) {
                return parseLayout(layout);
            }
        }
        throw new IllegalArgumentException(
                "Unknown template "+type+" in config "
                        +StringUtils.abbreviate(config.toString(), 500));
    }

    protected LayoutBase createLayoutForTemplateType(String type, JSONObject config) throws JSONException {
        if (type.equals("TextControl")) {
            return parseTextControl(config, type, new Callable<TextControlTemplate>() {
                @Override
                public TextControlTemplate call() throws Exception {
                    return new TextControlTemplate(mContext, mController, mArgController);
                }
            });
        }
        if ("Button".equals(type)) {
            if (config.getJSONObject("details").has("imageId")) {
                return parseButton(config);
            } else {
                return parseButtonText(config);
            }
        }
        if ("HorizontalRule".equals(type)) {
            return parseHorizontalRule();
        }
        if ("Image".equals(type) || "ImageControl".equals(type)) {
            return parseImage(config);
        }
        if ("Subview".equals(type)) {
            // Fetch what our content type is
            String contentType = mArgController.getArgs().getContentType();
            // Fetch link page id from details
            JSONObject details = config.optJSONObject("details");

            // generate the default value for default content type
            Iterator<String> keys = details.keys();
            String defaultContentType=keys!=null?details.getString(keys.next()):null;

            String pageLinkId = details.optString(contentType, defaultContentType);
            // Parse layout
            JSONObject pageConfig = mLayoutConfig.getLayout(pageLinkId);
            return parseConfig(pageConfig);
        }

        return null;
    }

    private void setCommonElements(JSONObject config, LayoutBase parsed) throws JSONException {
        // Set common elements of LayoutBase
        if (config.has("size")) {
            parsed.setLayoutSize((float) config.getDouble("size"));
        }
        if (config.has("weight")) {
            parsed.setWeight((float) config.getDouble("weight"));
        }

        // padding
        int padding = dipToPx(config.optInt("padding"));
        int paddingLeft = padding;
        int paddingTop = padding;
        int paddingRight = padding;
        int paddingBottom = padding;

        if (config.has("paddingLeft")) {
            paddingLeft = dipToPx(config.getInt("paddingLeft"));
        }

        if (config.has("paddingTop")) {
            paddingTop = dipToPx(config.getInt("paddingTop"));
        }

        if (config.has("paddingRight")) {
            paddingRight = dipToPx(config.getInt("paddingRight"));
        }

        if (config.has("paddingBottom")) {
            paddingBottom = dipToPx(config.getInt("paddingBottom"));
        }
        parsed.setPadding(new Padding(paddingLeft, paddingTop, paddingRight, paddingBottom));

        String height = config.optString("height", null);
        if (height != null) {
            parsed.setHeight(sizeDescToInt(height));
        }

        String width = config.optString("width", null);
        if (width != null) {
            parsed.setWidth(sizeDescToInt(width));
        }

        parsed.setToggleVisibilityOnParentClick(config.optBoolean("toggleVisibilityOnParentClick", false));

        String visibility = config.optString("visibility", null);
        if (visibility != null) {
            parsed.setLayoutVisible(visibilityDescToBool(visibility));
        }

        String backgroundColor = config.optString("backgroundColor", null);
        if (backgroundColor != null) {
            parsed.setBackgroundColor(Color.parseColor(backgroundColor));
        }

        parsed.setLayoutGravity(allGravityDescsToInt(config.optString("layoutGravity")));

        JSONObject details = config.optJSONObject("details");
        if (details != null) {
            parsed.setAction(details.optString("action", null));
            parsed.setPageLinkId(details.optString("pageLinkId", null));
        }
    }

    private boolean visibilityDescToBool(String visibility) {
        if ("visible".equals(visibility)) {
            return true;
        }
        if ("gone".equals(visibility)) {
            return false;
        }
        // Invisible not supported.
        throw new IllegalArgumentException("Unknown visibility "+visibility);
    }

    private int sizeDescToInt(String size) {
        if ("match_parent".equals(size)) {
            return ViewGroup.LayoutParams.MATCH_PARENT;
        }
        if ("wrap_content".equals(size)) {
            return ViewGroup.LayoutParams.WRAP_CONTENT;
        }
        return dipToPx(Integer.parseInt(size));
    }

    protected int dipToPx(int dip) {
        int px = (int) (dip * mScreenSize.getDensityRatio());
        if (px == 0 && dip != 0) {
            // On low density devices 1dp may be converted to 0 instead 1px.
            px = dip;
        }
        return px;
    }

    protected TextControlTemplate parseTextControl(JSONObject config, String type, Callable<TextControlTemplate> callable)
            throws JSONException {

        TextControlTemplate template = null;// new TextControlTemplate(mContext, mController, mArgController);
        try {
            template = callable.call();
        } catch (Exception e) {
            Log.error(e);
        }
        JSONObject details = config.optJSONObject("details");
        JSONObject headingText = config.optJSONObject("headingText");

        parseTextDetails(template, details,headingText);
        return template;
    }

    /**
     *
     * @param template
     * @param details
     * @param headingText can be null!
     * @throws JSONException
     */
    protected void parseTextDetails(TextControlTemplate template, JSONObject details,JSONObject headingText) throws JSONException {
        if(!details.isNull("text")) {
            template.setText(details.getString("text"));
        }

        int fontColor=Color.YELLOW;
        if(!details.isNull("fontColor")) {
            String fontColorStr = details.getString("fontColor");
            fontColor=Color.parseColor(fontColorStr);
        }
        template.setFontColor(fontColor);
        if(!details.isNull("fontFamily")) {
            template.setFontFamily(details.getString("fontFamily"));
        }
        //TODO REMOVE THE DEFAULT SIZE
        float fontSize=11;
        if(!details.isNull("fontSize")) {
            fontSize=((float) details.getDouble("fontSize"));
        }
        template.setFontSize(fontSize);

        if(!details.isNull("fontStyle")) {
            String style = details.optString("fontStyle");

            if (style.equals("normal")) {
                template.setFontStyle(Typeface.NORMAL);
            }
            if (style.equals("bold")) {
                template.setFontStyle(Typeface.BOLD);
            }
            if (style.equals("italic")) {
                template.setFontStyle(Typeface.ITALIC);
            }
            if (style.contains("bold") && style.contains("italic")) {
                template.setFontStyle(Typeface.BOLD_ITALIC);
            }
        }

        template.setGravity(allGravityDescsToInt(details.optString("gravity")));
    }

    protected int allGravityDescsToInt(String gravityDescs) {
        int gravity = Gravity.NO_GRAVITY;
        for (String desc : gravityDescs.split("\\|")) {
            if (desc.length() > 0) {
                gravity |= gravityDescToInt(desc);
            }
        }
        return gravity;
    }

    private int gravityDescToInt(String gravityDesc) {
        if ("center".equals(gravityDesc)) {
            return Gravity.CENTER;
        }
        if ("center_horizontal".equals(gravityDesc)) {
            return Gravity.CENTER_HORIZONTAL;
        }
        if ("center_vertical".equals(gravityDesc)) {
            return Gravity.CENTER_VERTICAL;
        }
        if ("left".equals(gravityDesc)) {
            return Gravity.LEFT;
        }
        if ("top".equals(gravityDesc)) {
            return Gravity.TOP;
        }
        if ("right".equals(gravityDesc)) {
            return Gravity.RIGHT;
        }
        if ("bottom".equals(gravityDesc)) {
            return Gravity.BOTTOM;
        }
        throw new IllegalArgumentException("Unknown gravity "+gravityDesc);
    }

    private ButtonTemplate parseButton(JSONObject config) throws JSONException {
        ButtonTemplate template = new ButtonTemplate(getContext(), getController(), mArgController);
        JSONObject details = config.getJSONObject("details");

        template.setImageId(details.getString("imageId"));

        return template;
    }

    private ButtonTextTemplate parseButtonText(JSONObject config) throws JSONException {
        ButtonTextTemplate template = new ButtonTextTemplate(getContext(), getController(), mArgController);
        JSONObject details = config.getJSONObject("details");
        parseTextDetails(template, details,null);
        return template;
    }

    private LayoutBase parseHorizontalRule() {
        return new HorizontalRuleTemplate(mContext, mController, mArgController);
    }

    private LayoutBase parseImage(JSONObject config) throws JSONException {
        ImageTemplate template = new ImageTemplate(getContext(), getController(), mArgController);
        JSONObject details = config.getJSONObject("details");
        template.setImageId(details.getString("imageId"));
        return template;
    }

}
