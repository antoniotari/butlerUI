package com.mokalab.butler.ui.templates;

import com.mokalab.butler.image.DisplaySize;

import org.apache.commons.lang3.Validate;
import org.jetbrains.annotations.NotNull;

import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import javax.inject.Inject;

import static com.mokalab.butler.injection.ApplicationGraph.getObjectGraph;

/**
 * Shared components by the layouts controls on a page.
 * Simplifies the constructor arguments of LayoutBase.
 *
 * Created by josh.allen@digiflare.com on 14-12-17
 */
public class LayoutController {

    private final FragmentManager mManager;
    private final ViewGroup mRootView;
    private final TemplateActionListener mActionListener;
    private final ThemePageConfig mTheme;
    @Inject
    DisplaySize mScreenSize;

    public LayoutController(ViewGroup rootView,
                            FragmentManager manager,
                            TemplateActionListener actionListener,
                            ThemePageConfig theme) {
        getObjectGraph().inject(this);
        Validate.notNull(rootView, "rootView");
        Validate.notNull(manager, "manager");
        Validate.notNull(actionListener, "actionListener");
        mRootView = rootView;
        mManager = manager;
        mActionListener = actionListener;
        mTheme = theme;
    }

    public View getRootView() {
        return mRootView;
    }

    public FragmentManager getManager() {
        return mManager;
    }

    @NotNull
    public TemplateActionListener getActionListener() {
        return mActionListener;
    }

    public ThemePageConfig getTheme() {
        return mTheme;
    }

    /**
     * @return The views width minus any padding
     */
    public int getViewWidth(View parent) {
        Validate.notNull(parent, "parent");
        int paddingLeft = parent.getPaddingLeft();
        int paddingRight = parent.getPaddingRight();
        // Get all of the padding of the given parent up to the root.
        while (parent != mRootView) {
            ViewParent actualParent = parent.getParent();
            if (actualParent == null){
                // The parent has not been attached to the root yet.
                actualParent = mRootView;
            }
            parent = (View) actualParent;
            paddingLeft += parent.getPaddingRight();
            paddingRight += parent.getPaddingRight();
        }
        return mScreenSize.getWidth() - paddingLeft - paddingRight;
    }

}