package com.mokalab.butler.ui.templates;

import android.content.Context;
import android.content.res.Resources;

import com.mokalab.butler.error.ErrorInfo;
import com.mokalab.butler.error.ErrorMessageMap;
import com.mokalab.butler.error.ErrorUserMessage;
import com.mokalab.butler.injection.ForApplication;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.inject.Inject;

import com.mokalab.butler.ui.R;

import static com.mokalab.butler.injection.ApplicationGraph.getObjectGraph;

/**
 * Created by josh.allen@digiflare.com on 15-01-09
 */
public class ErrorInfoTextReplacement implements TextReplacement {

    private final ErrorInfo mError;
    @Inject @ForApplication
    Context mContext;
    @Inject
    ErrorMessageMap mMessageMap;

    public ErrorInfoTextReplacement(ErrorInfo error) {
        getObjectGraph().inject(this);
        mError = error;
    }

    @Override
    public String getReplacementFor(String name) {
        if (mError == null) {
            return null;
        }
        if (":errorTitle/".equals(name) || ":dialogTitle/".equals(name) ) {
            return getUserMessage().getDialogTitle();
        }

        if (":errorUserMessage/".equals(name) || ":dialogMessage/".equals(name)) {
            return getUserMessage().getUserMessage();
        }
        if (":errorCode/".equals(name) && mError.getErrorCode() != 0) {
            try {
                return mContext.getResources().getResourceEntryName(mError.getErrorCode());
            } catch (Resources.NotFoundException ex) {
                return String.valueOf(mError.getErrorCode());
            }
        }
        if (":errorDebugMessage/".equals(name)) {
            return mError.getDebugMessage();
        }
        if (":errorContextData/".equals(name) && mError.getContextData() != null) {
            return mError.getContextData().toString();
        }
        if (":errorStackTrace/".equals(name) && mError.getException() != null) {
            return ExceptionUtils.getStackTrace(mError.getException());
        }
        return null;
    }

    private ErrorUserMessage getUserMessage() {
        ErrorUserMessage message = mMessageMap.getUserMessageForError(mError);
        if (message != null) {
            return message;
        }
        String title = mContext.getResources().getString(R.string.dialog_generalError_genericTitle);
        String userMessage;
        if (StringUtils.isEmpty(mError.getUserMessage())) {
            userMessage = mContext.getResources().getString(R.string.dialog_generalError_genericMessage);
        } else {
            userMessage = mError.getUserMessage();
        }
        return new ErrorUserMessage(title, userMessage);
    }
}
