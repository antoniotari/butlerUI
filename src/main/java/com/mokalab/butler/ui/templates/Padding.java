package com.mokalab.butler.ui.templates;

import android.view.View;

import com.mokalab.butler.util.ShortToStringStyle;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
* Created by jallen on 2015-01-03.
*/
public class Padding {
    public int mPaddingLeft;
    public int mPaddingTop;
    public int mPaddingRight;
    public int mPaddingBottom;

    public Padding(int paddingLeft, int paddingTop, int paddingRight, int paddingBottom) {
        mPaddingLeft = paddingLeft;
        mPaddingTop = paddingTop;
        mPaddingRight = paddingRight;
        mPaddingBottom = paddingBottom;
    }

    public Padding(View view) {
        this(view.getPaddingLeft(), view.getPaddingTop(),
                view.getPaddingRight(), view.getPaddingBottom());
    }

    public int getPaddingLeft() {
        return mPaddingLeft;
    }

    public int getPaddingTop() {
        return mPaddingTop;
    }

    public int getPaddingRight() {
        return mPaddingRight;
    }

    public int getPaddingBottom() {
        return mPaddingBottom;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, new ShortToStringStyle())
                .append(mPaddingLeft)
                .append(mPaddingTop)
                .append(mPaddingRight)
                .append(mPaddingBottom)
                .toString();
    }
}
