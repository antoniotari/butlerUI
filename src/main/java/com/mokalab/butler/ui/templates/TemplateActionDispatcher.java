package com.mokalab.butler.ui.templates;

import android.app.Activity;

import com.mokalab.butler.error.ErrorInfo;
import com.mokalab.butler.util.Log;

import org.apache.commons.lang3.Validate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Allows code to be modular when dealing with callbacks from dialogs and fragments that require the activity to
 * implement the listener interface.
 *
 * Created by josh.allen@digiflare.com on 15-01-29
 */
public class TemplateActionDispatcher implements TemplateActionListener {

    private final Activity mActivity;
    public Map<String,TemplateActionListener> mListeners = Collections.synchronizedMap(new HashMap<String,TemplateActionListener>());

    public TemplateActionDispatcher(Activity activity) {
        Validate.notNull(activity, "activity");
        mActivity = activity;
    }

    public void setListener(String requestId, TemplateActionListener listener) {
        Validate.notNull(requestId, "requestId");
        Validate.notNull(listener, "listener");
        mListeners.put(requestId, listener);
    }

    @Override
    public void onError(ErrorInfo error, TemplateArguments args) {
        TemplateActionListener listener = getListenerForRequest(error, args);
        if (listener != null) {
            listener.onError(error, args);
        }
    }

    @Override
    public void onAction(String actionId, TemplateArguments args) {
        TemplateActionListener listener = getListenerForRequest(actionId, args);
        if (listener != null) {
            listener.onAction(actionId, args);
        }
    }

    @Override
    public void openPageLink(String pageId, TemplateArguments args) {
        TemplateActionListener listener = getListenerForRequest(pageId, args);
        if (listener != null) {
            listener.openPageLink(pageId, args);
        }
    }

    @Override
    public void openDialogPageLink(String pageId, TemplateArguments args) {
        TemplateActionListener listener = getListenerForRequest(pageId, args);
        if (listener != null) {
            listener.openDialogPageLink(pageId, args);
        }
    }

    private TemplateActionListener getListenerForRequest(Object logObject, TemplateArguments args) {
        if (mActivity.isFinishing()) {
            Log.debug("Not dispatching", logObject, "because activity is finishing");
            return null;
        }
        if (args == null || args.getRequestId() == null) {
            Log.debug("No requestId for", logObject, "in args", args);
            return null;
        }
        TemplateActionListener listener = mListeners.get(args.getRequestId());
        if (listener == null) {
            Log.debug("No listener for", logObject, "with args", args);
            return null;
        }
        return listener;
    }
}
