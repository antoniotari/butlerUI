package com.mokalab.butler.ui.templates;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mokalab.butler.ui.R;
import com.mokalab.butler.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by josh.allen@digiflare.com on 15-02-17
 */
public class RelativeLayoutTemplate extends LayoutBase {

    private List<LayoutBase> mContent = new ArrayList<LayoutBase>();

    public RelativeLayoutTemplate(Context context, LayoutController controller, TemplateArgController argController) {
        super(context, controller, argController);
    }

    @Override
    protected View getViewNoPadding(int position, View convertView, ViewGroup parent) {
        if (convertView != null) {
            return convertView;
        }
        LayoutInflater inflater = LayoutInflater.from(getContext());
        RelativeLayout root = (RelativeLayout) inflater.inflate(R.layout.template_relative_layout, parent, false);

        for (LayoutBase layout : mContent) {
            for (int i = 0; i < layout.getCount(); i++) {
                View child = layout.getView(i, null, root);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) child.getLayoutParams();
                params = adjustLayoutParams(layout, params);
                root.addView(child, params);
            }
        }

        return root;
    }

    private RelativeLayout.LayoutParams adjustLayoutParams(LayoutBase layout, RelativeLayout.LayoutParams params) {
        for (Integer rule : gravityToRules(layout.getLayoutGravity())) {
            params.addRule(rule);
        }
        return params;
    }

    private List<Integer> gravityToRules(int gravity) {
        List<Integer> rules = new ArrayList<Integer>();
        if ((gravity & Gravity.BOTTOM) == Gravity.BOTTOM) {
            rules.add(RelativeLayout.ALIGN_PARENT_BOTTOM);
        }
        else if ((gravity & Gravity.TOP) == Gravity.TOP) {
            rules.add(RelativeLayout.ALIGN_PARENT_TOP);
        }
        else if ((gravity & Gravity.CENTER_VERTICAL) == Gravity.CENTER_VERTICAL) {
            rules.add(RelativeLayout.CENTER_VERTICAL);
        }

        if ((gravity & Gravity.LEFT) == Gravity.LEFT) {
            rules.add(RelativeLayout.ALIGN_PARENT_LEFT);
        }
        else if ((gravity & Gravity.RIGHT) == Gravity.RIGHT) {
            rules.add(RelativeLayout.ALIGN_PARENT_RIGHT);
        }
        else if ((gravity & Gravity.CENTER_HORIZONTAL) == Gravity.CENTER_HORIZONTAL) {
            rules.add(RelativeLayout.CENTER_HORIZONTAL);
        }

        if ((gravity & Gravity.CENTER) == Gravity.CENTER) {
            rules.add(RelativeLayout.CENTER_IN_PARENT);
        }

        return rules;
    }

    public void addContent(LayoutBase content) {
        content.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                Log.debug("onChanged");
                notifyDataSetChanged();
            }
        });
        mContent.add(content);
    }

    @Override
    public void onDataSetChanged(TemplateArguments controller) {
        // Space for rent.
    }
}