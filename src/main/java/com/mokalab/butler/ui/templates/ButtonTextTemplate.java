package com.mokalab.butler.ui.templates;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by josh.allen@digiflare.com on 15-01-12
 */
public class ButtonTextTemplate extends TextControlTemplate {

    public ButtonTextTemplate(Context context, LayoutController controller, TemplateArgController argController) {
        super(context, controller, argController);
    }

    @Override
    protected View getViewNoPadding(int position, View convertView, ViewGroup parent) {
        if (convertView != null) {
            return convertView;
        }
        View root = super.getViewNoPadding(position, convertView, parent);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processAction();
            }
        });
        return root;
    }

}
