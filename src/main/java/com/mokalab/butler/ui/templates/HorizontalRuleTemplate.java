package com.mokalab.butler.ui.templates;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mokalab.butler.ui.R;

/**
 * Created by josh.allen@digiflare.com on 15-01-13
 */
public class HorizontalRuleTemplate extends LayoutBase {
    public HorizontalRuleTemplate(Context context, LayoutController controller, TemplateArgController argController) {
        super(context, controller, argController);
    }

    @Override
    protected View getViewNoPadding(int position, View convertView, ViewGroup parent) {
        if (convertView != null) {
            return convertView;
        }
        LayoutInflater inflater = LayoutInflater.from(getContext());
        return inflater.inflate(R.layout.template_horizontal_rule, parent, false);
    }

    @Override
    public void onDataSetChanged(TemplateArguments controller) {
        // Space for rent.
    }
}
