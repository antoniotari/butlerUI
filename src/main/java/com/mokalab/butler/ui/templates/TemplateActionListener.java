package com.mokalab.butler.ui.templates;

import com.mokalab.butler.error.ErrorInfo;

/**
 * Created by josh.allen@digiflare.com on 14-12-22
 */
public interface TemplateActionListener {

    void onError(ErrorInfo error, TemplateArguments args);

    void onAction(String actionId, TemplateArguments args);

    void openPageLink(String pageId, TemplateArguments args);

    void openDialogPageLink(String pageId, TemplateArguments args);
}
