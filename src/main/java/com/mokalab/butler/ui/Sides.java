package com.mokalab.butler.ui;

/**
 * Sides of a box.
 *
 * Created by josh.allen@digiflare.com on 14-12-10
 */
public enum Sides {
    LEFT, TOP, RIGHT, BOTTOM
}
